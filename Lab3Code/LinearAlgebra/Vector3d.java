//Gabriel Internoscia 1842167

final class Vector3d{
    //Field initialization
    private double x;
    private double y;
    private double z;

    //Constructor
    public Vector3d(double x, double y, double z){
        this.x = x;
        this.y = y;
        this.z = z;
    }

    //Getter for x
    public double getx(){
        return this.x;
    }

    //Getter for y
    public double gety(){
        return this.y;
    }

    //Getter for z
    public double getz(){
        return this.z;
    }

    //Return magnitude
    public double magnitude(Vector3d vector){
        x = Math.pow(vector.getx(), 2);
        y = Math.pow(vector.gety(), 2);
        z = Math.pow(vector.getz(), 2);

        double magnitude = x+y+z;
        magnitude = Math.sqrt(magnitude);

        return magnitude;
    }

    //Return dotProduct
    public double dotProduct(Vector3d vector1, Vector3d vector2){
        x = vector1.getx() * vector2.getx();
        y = vector1.gety() * vector2.gety();
        z = vector1.getz() * vector2.getz();

        Double sum = x+y+z;
        return sum;
    }

    //Return sum of Vector3d's
    public Vector3d add(Vector3d vector1, Vector3d vector2){
        x = vector1.getx() + vector2.getx();
        y = vector1.gety() + vector2.gety();
        z = vector1.getz() + vector2.getz();

        Vector3d finalVector = new Vector3d(x, y, z);
        return finalVector;
    }
}