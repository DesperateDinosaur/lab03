//Gabriel Internoscia 1842167

import static org.junit.jupiter.api.Assertions.*;
import org.junit.Test;

public class Vector3dTests{
    //Creating two Vector3d objects
    Vector3d vector1 = new Vector3d(1, 2, 3);
    Vector3d vector2 = new Vector3d(3, 2, 1);

    //Testing Vector3d get method
    @Test
    public void testGetVector(){
        Vector3d vectorGet = new Vector3d(1, 2, 3);

        assertEquals(1, vectorGet.getx());
        assertEquals(2, vectorGet.gety());
        assertEquals(3, vectorGet.getz());
    }

    //Testing math methods
    @Test
    public void testMagnitude(){
        Vector3d vectorMag = new Vector3d(1, 2, 3);

        assertEquals(3.74, vectorMag.magnitude(vectorMag), 0.01);
    }
    @Test
    public void testDotProduct(){
        Vector3d vector1Dot = new Vector3d(1, 2, 3);
        Vector3d vector2Dot = new Vector3d(3, 2, 1);

        assertEquals(10, vector1Dot.dotProduct(vector1Dot, vector2Dot));
    }
    @Test
    public void testAddition(){
        Vector3d vector1Add = new Vector3d(1, 2, 3);
        Vector3d vector2Add = new Vector3d(3, 2, 1);

        Vector3d vectorExpected = new Vector3d(4, 4, 4);
        Vector3d vectorActual = vector1Add.add(vector1Add, vector2Add);

        assertEquals(vectorExpected.getx(), vectorActual.getx());
        assertEquals(vectorExpected.gety(), vectorActual.gety());
        assertEquals(vectorExpected.getz(), vectorActual.getz());
    }
}
